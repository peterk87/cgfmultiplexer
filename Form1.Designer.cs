﻿namespace PrimerMultiplexGrouper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMultiplexes = new System.Windows.Forms.RichTextBox();
            this.txtMultiplexThermScores = new System.Windows.Forms.RichTextBox();
            this.lblPrimerFile = new System.Windows.Forms.Label();
            this.btnGetMultiplexSolution = new System.Windows.Forms.Button();
            this.txtSeed = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumMP = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMPSize = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblScoreFile = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtThermThreshold = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtMultiplexes
            // 
            this.txtMultiplexes.Location = new System.Drawing.Point(12, 30);
            this.txtMultiplexes.Name = "txtMultiplexes";
            this.txtMultiplexes.Size = new System.Drawing.Size(416, 254);
            this.txtMultiplexes.TabIndex = 0;
            this.txtMultiplexes.Text = "";
            // 
            // txtMultiplexThermScores
            // 
            this.txtMultiplexThermScores.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMultiplexThermScores.Location = new System.Drawing.Point(434, 30);
            this.txtMultiplexThermScores.Name = "txtMultiplexThermScores";
            this.txtMultiplexThermScores.Size = new System.Drawing.Size(534, 379);
            this.txtMultiplexThermScores.TabIndex = 1;
            this.txtMultiplexThermScores.Text = "";
            // 
            // lblPrimerFile
            // 
            this.lblPrimerFile.AutoSize = true;
            this.lblPrimerFile.Location = new System.Drawing.Point(20, 287);
            this.lblPrimerFile.Name = "lblPrimerFile";
            this.lblPrimerFile.Size = new System.Drawing.Size(107, 13);
            this.lblPrimerFile.TabIndex = 2;
            this.lblPrimerFile.Text = "Drop Primer File Here";
            // 
            // btnGetMultiplexSolution
            // 
            this.btnGetMultiplexSolution.Location = new System.Drawing.Point(12, 304);
            this.btnGetMultiplexSolution.Name = "btnGetMultiplexSolution";
            this.btnGetMultiplexSolution.Size = new System.Drawing.Size(102, 40);
            this.btnGetMultiplexSolution.TabIndex = 3;
            this.btnGetMultiplexSolution.Text = "Get Multiplex Solution";
            this.btnGetMultiplexSolution.UseVisualStyleBackColor = true;
            // 
            // txtSeed
            // 
            this.txtSeed.Location = new System.Drawing.Point(120, 327);
            this.txtSeed.Name = "txtSeed";
            this.txtSeed.Size = new System.Drawing.Size(71, 20);
            this.txtSeed.TabIndex = 4;
            this.txtSeed.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(117, 311);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Seed for RNG";
            // 
            // txtNumMP
            // 
            this.txtNumMP.Location = new System.Drawing.Point(120, 366);
            this.txtNumMP.Name = "txtNumMP";
            this.txtNumMP.Size = new System.Drawing.Size(71, 20);
            this.txtNumMP.TabIndex = 6;
            this.txtNumMP.Text = "8";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(117, 350);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Multiplexes";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(232, 350);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Multiplex Size";
            // 
            // txtMPSize
            // 
            this.txtMPSize.Location = new System.Drawing.Point(235, 366);
            this.txtMPSize.Name = "txtMPSize";
            this.txtMPSize.Size = new System.Drawing.Size(71, 20);
            this.txtMPSize.TabIndex = 9;
            this.txtMPSize.Text = "5";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(12, 397);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(72, 13);
            this.lblStatus.TabIndex = 10;
            this.lblStatus.Text = "Current Seed:";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(12, 350);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(102, 33);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel! STOP!!!";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblScoreFile
            // 
            this.lblScoreFile.AutoSize = true;
            this.lblScoreFile.Location = new System.Drawing.Point(187, 287);
            this.lblScoreFile.Name = "lblScoreFile";
            this.lblScoreFile.Size = new System.Drawing.Size(106, 13);
            this.lblScoreFile.TabIndex = 12;
            this.lblScoreFile.Text = "Drop Score File Here";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(232, 311);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(193, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Tries Before Lowering Therm Threshold";
            // 
            // txtThermThreshold
            // 
            this.txtThermThreshold.Location = new System.Drawing.Point(235, 327);
            this.txtThermThreshold.Name = "txtThermThreshold";
            this.txtThermThreshold.Size = new System.Drawing.Size(71, 20);
            this.txtThermThreshold.TabIndex = 14;
            this.txtThermThreshold.Text = "3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 421);
            this.Controls.Add(this.txtThermThreshold);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblScoreFile);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.txtMPSize);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNumMP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSeed);
            this.Controls.Add(this.btnGetMultiplexSolution);
            this.Controls.Add(this.lblPrimerFile);
            this.Controls.Add(this.txtMultiplexThermScores);
            this.Controls.Add(this.txtMultiplexes);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtMultiplexes;
        private System.Windows.Forms.RichTextBox txtMultiplexThermScores;
        private System.Windows.Forms.Label lblPrimerFile;
        private System.Windows.Forms.Button btnGetMultiplexSolution;
        private System.Windows.Forms.TextBox txtSeed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumMP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMPSize;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblScoreFile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtThermThreshold;
    }
}

