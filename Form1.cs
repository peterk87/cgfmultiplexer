﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace PrimerMultiplexGrouper
{
    public partial class Form1 : Form
    {
        private readonly BackgroundWorker _bgw;
        private readonly Dictionary<string, string> _primerFile = new Dictionary<string, string>();
        private int _mpSize;
        private MultiplexGrouping _multiplexGrouping;
        private int _multiplexes;

        private List<string> _scoreLines = new List<string>();

        private int _seed;

        private int _thermThreshold = 3;


        public Form1()
        {
            InitializeComponent();

            _bgw = new BackgroundWorker
                       {
                           WorkerReportsProgress = true,
                           WorkerSupportsCancellation = true
                       };

            _bgw.DoWork += (sender, args) => GetMultiplexSolution();
            _bgw.ProgressChanged += (sender, args) => ReportProgress(args);
            _bgw.RunWorkerCompleted += (sender, args) => ReportSolution();

            btnCancel.Click += (sender, args) => CancelSearching();

            btnCancel.Enabled = false;
            btnGetMultiplexSolution.Enabled = false;

            lblScoreFile.AllowDrop = true;
            lblScoreFile.DragOver += (sender, args) => args.Effect = DragDropEffects.All;
            lblScoreFile.DragEnter += (sender, args) => args.Effect = DragDropEffects.All;

            lblScoreFile.DragDrop += (sender, args) => OnScoreFileDrop(args);

            lblPrimerFile.AllowDrop = true;
            lblPrimerFile.DragEnter += (sender, args) => args.Effect = DragDropEffects.All;
            lblPrimerFile.DragOver += (sender, args) => args.Effect = DragDropEffects.All;
            lblPrimerFile.DragDrop += (sender, args) => OnPrimerFileDrop(args);


            txtThermThreshold.KeyPress += (sender, args) => OnThermThresholdKeyPress(args);
            txtThermThreshold.LostFocus += (sender, args) => OnThermThresholdKeyPress(new KeyPressEventArgs((char) Keys.Enter));

            btnGetMultiplexSolution.Click += (sender, args) =>
                                                 {
                                                     _seed = Convert.ToInt32(txtSeed.Text);
                                                     _mpSize = Convert.ToInt32(txtMPSize.Text);
                                                     _multiplexes = Convert.ToInt32(txtNumMP.Text);
                                                     btnGetMultiplexSolution.Enabled = false;
                                                     btnCancel.Enabled = true;
                                                     _bgw.RunWorkerAsync();
                                                 };
        }

        private void OnThermThresholdKeyPress(KeyPressEventArgs args)
        {
            int i;
            if (int.TryParse(args.KeyChar.ToString(), out i) || args.KeyChar == '\b')
            {
                args.Handled = false;
            }
            else if (args.KeyChar == '\t' || args.KeyChar == (char) Keys.Enter)
            {
                if (int.TryParse(txtThermThreshold.Text, out i))
                {
                    _thermThreshold = i;
                }
            }
            else
            {
                args.Handled = true;
            }
        }

        private void CancelSearching()
        {
            _bgw.CancelAsync();
            btnGetMultiplexSolution.Enabled = true;
            btnCancel.Enabled = false;

            lblStatus.Text += "::Searching Stopped!";
        }

        private void ReportSolution()
        {
            btnGetMultiplexSolution.Enabled = true;
            btnCancel.Enabled = false;
            if (_multiplexGrouping.Solutions.Count > 0)
            {
                lblStatus.Text = string.Format("Current Seed: {0}", _multiplexGrouping.Seed);
                WriteSolutions();
            }
            else
            {
                lblStatus.Text = string.Format("No solution found! Current Seed: {0}", _multiplexGrouping.Seed);
            }
        }

        private void ReportProgress(ProgressChangedEventArgs args)
        {
            if (args.UserState != null)
            {
                if (args.UserState is int)
                {
                    var seed = (int) args.UserState;
                    lblStatus.Text = string.Format("Current Seed: {0}", seed);
                }
                else if (args.UserState is string)
                {
                    var text = (string) args.UserState;
                    txtMultiplexThermScores.Text = text;
                }
            }
        }

        private void OnPrimerFileDrop(DragEventArgs args)
        {
            if (!args.Data.GetDataPresent(DataFormats.FileDrop)) return;
            var files = (string[]) args.Data.GetData(DataFormats.FileDrop);
            using (var sr = new StreamReader(files[0]))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (string.IsNullOrEmpty(line)) continue;
                    string[] split = line.Split('\t');
                    _primerFile.Add(split[0], line);
                }
            }
            lblPrimerFile.Text = string.Format("{0} primer file lines read", _primerFile.Count);
            btnGetMultiplexSolution.Enabled = _primerFile.Count > 0 && _scoreLines.Count > 0;
        }

        private void OnScoreFileDrop(DragEventArgs args)
        {
            if (!args.Data.GetDataPresent(DataFormats.FileDrop)) return;
            var files = (string[]) args.Data.GetData(DataFormats.FileDrop);

            if (files.Length != 1) return;
            var scoreFile = new FileInfo(files[0]);

            //var newScoreFile = scoreFile.FullName + ".new";

            //list to store lines of non-redundant thermodynamic interactions
            var list = new List<string>();

            using (var sr = new StreamReader(scoreFile.FullName))
                //using (var sw = new StreamWriter(newScoreFile))
            {
                //sw.WriteLine( sr.ReadLine());
                sr.ReadLine();

                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();

                    if (string.IsNullOrEmpty(line)) continue;


                    string[] split = line.Split('\t');

                    string first = split[0];
                    string second = split[1];


                    int lastDash1 = first.LastIndexOf('-');
                    string id1 = first.Remove(lastDash1);
                    string size1 = first.Substring(lastDash1);
                    int lastDash2 = second.LastIndexOf('-');
                    string id2 = second.Remove(lastDash2);
                    string size2 = second.Substring(lastDash2);

                    if (id1 != id2 && size1 != size2)
                        list.Add(line);
                    //sw.WriteLine(line);
                    //
                }
            }
            _scoreLines = list;
            lblScoreFile.Text = string.Format("{0} pairwise scores saved", _scoreLines.Count);

            btnGetMultiplexSolution.Enabled = _primerFile.Count > 0 && _scoreLines.Count > 0;
        }

        private void GetMultiplexSolution()
        {
            if (_scoreLines.Count == 0) return;

            _multiplexGrouping = new MultiplexGrouping(_scoreLines, _multiplexes, _mpSize, _bgw, _seed, _thermThreshold);


            while (!_multiplexGrouping.Solved)
            {
                if (_multiplexGrouping.TryGetSolution()) break;
                _multiplexGrouping.Seed++;
                _bgw.ReportProgress(0, _multiplexGrouping.Seed);
            }
        }

        private void WriteSolutions()
        {
            var sb = new StringBuilder();
            var sbTherm = new StringBuilder();

            var dictTherms = new Dictionary<ThermScore, int>();


            int solutionCount = 1;
            foreach (MultiplexGroups multiplexGroups in _multiplexGrouping.Solutions)
            {
                string solutionString = string.Format("Solution {0}", solutionCount);
                sb.AppendLine(solutionString);
                sbTherm.AppendLine(solutionString);
                int multiplexCount = 1;
                foreach (MultiplexGroup multiplex in multiplexGroups.Multiplexes)
                {
                    string multiplexString = string.Format("Multiplex {0}", multiplexCount);
                    sb.AppendLine(multiplexString);
                    sbTherm.AppendLine(multiplexString);

                    foreach (var pair in multiplex.MarkerSizeDict)
                    {
                        sb.AppendLine(string.Format("{0}\t{1}\t{2}", pair.Value, pair.Key, _primerFile[string.Format("{0}-{1}", pair.Key, pair.Value)]));
                        sbTherm.Append(pair.Key);
                        foreach (var pair2 in multiplex.MarkerSizeDict)
                        {
                            sbTherm.Append("\t");
                            Dictionary<int, Dictionary<string, Dictionary<int, Thermodynamics>>> dictSize1;
                            if (!_multiplexGrouping.MarkerMarkerDict2.TryGetValue(pair.Key, out dictSize1)) continue;
                            Dictionary<string, Dictionary<int, Thermodynamics>> dictPair2;
                            if (dictSize1.TryGetValue(pair.Value, out dictPair2))
                            {
                                Dictionary<int, Thermodynamics> dictSize2;
                                if (dictPair2.TryGetValue(pair2.Key, out dictSize2))
                                {
                                    Thermodynamics thermScores;
                                    if (dictSize2.TryGetValue(pair2.Value, out thermScores))
                                    {
                                        if (dictTherms.ContainsKey(thermScores.LowestThermScore))
                                        {
                                            dictTherms[thermScores.LowestThermScore]++;
                                        }
                                        else
                                        {
                                            dictTherms.Add(thermScores.LowestThermScore, 1);
                                        }

                                        sbTherm.Append(thermScores.LowestThermScore);
                                    }
                                }
                            }
                        }
                        sbTherm.AppendLine();
                    }
                    sbTherm.AppendLine();

                    multiplexCount++;
                }
                sb.AppendLine();
                sbTherm.AppendLine();
                solutionCount++;
            }

            txtMultiplexes.Text = sb.ToString();
            txtMultiplexThermScores.Text = sbTherm.ToString();

            var thermStatus = new StringBuilder();

            foreach (var pair in dictTherms)
            {
                thermStatus.Append("::");
                thermStatus.Append(pair.Key);
                thermStatus.Append("=");
                thermStatus.Append(pair.Value);
            }

            lblStatus.Text += thermStatus.ToString();
        }
    }
}

/*
 * 
 * 
 * Give each thermodynamic value a score for each parameter - PrimPrimEnd2,	PrimPrimEnd1,	PrimPrimAny,	PrimProdEnd1,	PrimProdAny
'Determine the thermodynamic score of each parameter that was tested with the primer pairs and their products
'1 is the highest compatibility of two primers and 4 being the lowest compatibility based on thermodynamics
            tm = 0
            Select Case p1
                Case Is >= -2
                    th(0) = 1
                Case -4 To -2
                    th(0) = 2
                Case -6 To -4
                    th(0) = 3
                Case Else
                    th(0) = 4
            End Select
            
            Select Case p2
                Case Is >= -6
                    th(1) = 1
                Case -8 To -6
                    th(1) = 2
                Case -10 To -8
                    th(1) = 3
                Case Else
                    th(1) = 4
            End Select
            
            Select Case p3
                Case Is >= -6
                    th(2) = 1
                Case -8 To -6
                    th(2) = 2
                Case -10 To -8
                    th(2) = 3
                Case Else
                    th(2) = 4
            End Select
    
            Select Case p4
                Case Is >= -10
                    th(3) = 1
                Case -12 To -10
                    th(3) = 2
                Case -14 To -12
                    th(3) = 3
                Case Else
                    th(3) = 4
            End Select
    
            Select Case p5
                Case Is >= -10
                    th(4) = 1
                Case -12 To -10
                    th(4) = 2
                Case -14 To -12
                    th(4) = 3
                Case Else
                    th(4) = 4
            End Select
            
'The largest thermodynamic score is used of all 5 parameters being tested for the primer pairs
*/