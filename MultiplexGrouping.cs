﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PrimerMultiplexGrouper
{
    public class MultiplexGrouping
    {
        private readonly BackgroundWorker _bgw;
        private readonly List<int> _markerIndices;

        /// <summary>Primer 1, Primer 2, Primer 1 Size, Primer 2 Size, Thermodynamics</summary>
        private readonly Dictionary<string, Dictionary<string, Dictionary<int, Dictionary<int, Thermodynamics>>>>
            _markerMarkerDict =
                new Dictionary<string, Dictionary<string, Dictionary<int, Dictionary<int, Thermodynamics>>>>();

        /// <summary>Primer 1, Primer Size 1, Primer 2, Primer 2 Size, Thermodynamics</summary>
        private readonly Dictionary<string, Dictionary<int, Dictionary<string, Dictionary<int, Thermodynamics>>>>
            _markerMarkerDict2 =
                new Dictionary<string, Dictionary<int, Dictionary<string, Dictionary<int, Thermodynamics>>>>();

        private readonly Dictionary<string, HashSet<int>> _markerSizeDict = new Dictionary<string, HashSet<int>>();

        private readonly List<string> _markers = new List<string>();
        private readonly int _multiplexSize;
        private readonly int _multiplexes;

        private readonly HashSet<int> _sizeHash = new HashSet<int>();

        private readonly Dictionary<int, HashSet<string>> _sizeMarkerDict = new Dictionary<int, HashSet<string>>();
        private readonly List<int> _sizes = new List<int>();

        private readonly List<MultiplexGroups> _solutions = new List<MultiplexGroups>();
        private readonly HashSet<string> _unusedMarkers = new HashSet<string>();
        private readonly HashSet<string> _usedMarkers = new HashSet<string>();
        public int Seed;
        public int TriesBeforeLoweringThermThreshold;

        private int _bestMultiplexCount;
        private List<int> _bestMultiplexesHigh = new List<int>();
        private List<int> _bestMultiplexesMedium = new List<int>();
        private ThermScore _minThermScore = ThermScore.High;
        private int[][] _sizeMatrix;
        private int[][] _solutionMatrix;
        private bool _solved;

        public MultiplexGrouping(List<string> scoreFileLines, int multiplexes, int multiplexSize, BackgroundWorker bgw, int seed, int triesBeforeLoweringThermThreshold)
        {
            _multiplexes = multiplexes;
            _multiplexSize = multiplexSize;
            Seed = seed;
            TriesBeforeLoweringThermThreshold = triesBeforeLoweringThermThreshold;
            _bgw = bgw;
            _markerIndices = InitMultiplexGrouping(scoreFileLines);
        }

        public Dictionary<string, Dictionary<string, Dictionary<int, Dictionary<int, Thermodynamics>>>> MarkerMarkerDict { get { return _markerMarkerDict; } }
        public Dictionary<string, Dictionary<int, Dictionary<string, Dictionary<int, Thermodynamics>>>> MarkerMarkerDict2 { get { return _markerMarkerDict2; } }
        public Dictionary<string, HashSet<int>> MarkerSizeDict { get { return _markerSizeDict; } }
        public List<string> Markers { get { return _markers; } }
        public HashSet<int> SizeHash { get { return _sizeHash; } }
        public Dictionary<int, HashSet<string>> SizeMarkerDict { get { return _sizeMarkerDict; } }
        public List<int> Sizes { get { return _sizes; } }
        public int[][] SolutionMatrix { get { return _solutionMatrix; } }
        public HashSet<string> UnusedMarkers { get { return _unusedMarkers; } }
        public int BestMultiplexCount { get { return _bestMultiplexCount; } }
        public List<int> BestMultiplexesHigh { get { return _bestMultiplexesHigh; } }
        public List<int> BestMultiplexesMedium { get { return _bestMultiplexesMedium; } }
        public ThermScore MinThermScore { get { return _minThermScore; } }
        public bool Solved { get { return _solved; } }
        public HashSet<string> UsedMarkers { get { return _usedMarkers; } }
        public List<MultiplexGroups> Solutions { get { return _solutions; } }
        public int[][] SizeMatrix { get { return _sizeMatrix; } }

        public bool TryGetSolution()
        {
            _minThermScore = ThermScore.High;
            _bestMultiplexCount = 0;
            var r = new Random(Seed);
            Pick(0, 0, r, _markerIndices, r.NextDouble() > 0.5 ? _sizes[0] : _sizes[1]);
            return _bgw.CancellationPending;
        }

        private List<int> InitMultiplexGrouping(List<string> scoreFileLines)
        {
            foreach (string line in scoreFileLines)
            {
                string[] split = line.Split('\t');

                string first = split[0];
                string second = split[1];


                int lastDash1 = first.LastIndexOf('-');
                string id1 = first.Remove(lastDash1);
                int size1 = Convert.ToInt32(first.Substring(lastDash1 + 1));
                int lastDash2 = second.LastIndexOf('-');
                string id2 = second.Remove(lastDash2);
                int size2 = Convert.ToInt32(second.Substring(lastDash2 + 1));
                //PrimPrimEnd2,	PrimPrimEnd1,	PrimPrimAny,	PrimProdEnd1,	PrimProdAny
                double primPrimEnd2 = Convert.ToDouble(split[2]);
                double primPrimEnd1 = Convert.ToDouble(split[3]);
                double primPrimAny = Convert.ToDouble(split[4]);
                double primProdEnd1 = Convert.ToDouble(split[5]);
                double primProdAny = Convert.ToDouble(split[6]);


                _unusedMarkers.Add(id1);
                _unusedMarkers.Add(id2);

                _sizeHash.Add(size1);
                _sizeHash.Add(size2);


                AddMarkerSizesToDicts(size2, id2, size1, id1);


                AddPrimerPrimerThermodynamicsToDict(primPrimAny, primProdEnd1, primPrimEnd1, primPrimEnd2, primProdAny, id2, id1, size2, size1);
                AddPrimerPrimerThermodynamicsToDict2(primPrimAny, primProdEnd1, primPrimEnd1, primPrimEnd2, primProdAny, id2, id1, size2, size1);
            }

            foreach (int i in _sizeHash)
            {
                _sizes.Add(i);
            }

            _sizes.Sort();


            foreach (string marker in _unusedMarkers)
            {
                _markers.Add(marker);
            }

            _markers.Sort();

            _solutionMatrix = new int[_multiplexes][];
            _sizeMatrix = new int[_multiplexes][];

            for (int i = 0; i < _solutionMatrix.Length; i++)
            {
                _solutionMatrix[i] = new int[_multiplexSize]; //[_sizes.Count];
                _sizeMatrix[i] = new int[_multiplexSize];
                for (int j = 0; j < _solutionMatrix[i].Length; j++)
                {
                    _solutionMatrix[i][j] = -1;
                    _sizeMatrix[i][j] = -1;
                }
            }

            _solved = false;

            var markerIndexList = new List<int>();
            for (int i = 0; i < _markers.Count; i++)
            {
                markerIndexList.Add(i);
            }
            return markerIndexList;
        }

        //Pick - Sub for picking the genes to be present in each element of the multiplex solution with recursion and backtracing until suitable solution is found
        //c - Column number referring to x in matrix(x,y)
        //r - Row number referring to y in matrix(x,y)
        private void Pick(int multiplexIndex, int sizeIndex, Random random, List<int> markerIndexList, int size)
        {
            if (_bgw.CancellationPending)
                return;
            //temporary holder for backtracing
            if (_solved)
                return;

            var multiplexMarkerHash = new HashSet<int>();


            foreach (var t in _solutionMatrix)
            {
                foreach (int t1 in t)
                {
                    if (t1 != -1)
                    {
                        multiplexMarkerHash.Add(t1);
                    }
                }
            }

            var markersLeftHash = new List<int>();
            for (int i = 0; i < _markers.Count; i++)
            {
                if (multiplexMarkerHash.Contains(i)) continue;
                markersLeftHash.Add(i);
            }


            if (markersLeftHash.Count == 0)
            {
                _solved = true;

                _solutions.Add(new MultiplexGroups(this));

                return;
            }

            int randomStartForMarkerSearch = random.Next(0, markersLeftHash.Count);

            //randomly select spot to start looking in genes
            int currentCandidateMarker = randomStartForMarkerSearch;
            do
            {
                //check if pick is suitable for multiplex solution 
                if (_sizeMarkerDict[size].Contains(_markers[markerIndexList[currentCandidateMarker]]))
                    if (!multiplexMarkerHash.Contains(currentCandidateMarker))
                        if (CheckMarkerAgainstMultiplex(multiplexIndex, sizeIndex, currentCandidateMarker, size))
                        {
                            int tempMarkerPlaceHolder = _solutionMatrix[multiplexIndex][sizeIndex];
                            int tempSizePlaceHolder = _sizeMatrix[multiplexIndex][sizeIndex];
                            //temporarily store the previous value in the current solution matrix in t, a temporary variable
                            _solutionMatrix[multiplexIndex][sizeIndex] = currentCandidateMarker;
                            _sizeMatrix[multiplexIndex][sizeIndex] = size;
                            //i, representing a gene pick that is suitable is placed within the current spot within the solution matrix
                            //if the current column number, c, within the solution matrix reaches the upper bound of the columns 


                            if (sizeIndex == _multiplexSize - 1) //number of multiplexes
                            {
                                var multiplexMarkerList = new List<int>();

                                foreach (var ints in _solutionMatrix)
                                {
                                    bool fullMultiplex = true;
                                    foreach (int i in ints)
                                    {
                                        if (i == -1)
                                        {
                                            fullMultiplex = false;
                                            break;
                                        }
                                    }
                                    if (fullMultiplex)
                                        multiplexMarkerList.AddRange(ints);
                                }

                                if (_minThermScore == ThermScore.High)
                                    if (_bestMultiplexesHigh.Count < multiplexMarkerList.Count)
                                    {
                                        _bestMultiplexesHigh = multiplexMarkerList;
                                        _bestMultiplexCount = 0;
                                    }
                                    else if (_bestMultiplexesHigh.Count == multiplexMarkerList.Count)
                                    {
                                        _bestMultiplexCount++;
                                    }

                                if (_minThermScore == ThermScore.Normal)
                                    if (_bestMultiplexesMedium.Count < multiplexMarkerList.Count)
                                    {
                                        _bestMultiplexesMedium = multiplexMarkerList;
                                        _bestMultiplexCount = 0;
                                    }
                                    else if (_bestMultiplexesMedium.Count == multiplexMarkerList.Count)
                                    {
                                        _bestMultiplexCount++;
                                    }

                                if (_bestMultiplexCount == TriesBeforeLoweringThermThreshold)
                                {
                                    _minThermScore++;
                                    _bestMultiplexCount = 0;
                                }

                                //write multiplex solution so far to stringbuilder
//                                 var sb = new StringBuilder();
// 
//                                 for (int i = 0; i < _solutionMatrix.Length; i++)
//                                 {
//                                     string multiplexString = string.Format("Multiplex {0}", i);
//                                     sb.AppendLine(multiplexString);
//                                     int[] multiplex = _solutionMatrix[i];
//                                     int[] mpSizes = _sizeMatrix[i];
//                                     for (int j = 0; j < multiplex.Length; j++)
//                                     {
//                                         int index = multiplex[j];
//                                         if (index == -1) continue;
//                                         string marker1 = _markers[index];
//                                         int size1 = mpSizes[j];
//                                         sb.Append(marker1);
// 
//                                         for (int k = 0; k < multiplex.Length; k++)
//                                         {
//                                             int index2 = multiplex[k];
//                                             if (index2 == -1) continue;
//                                             sb.Append("\t");
//                                             string marker2 = _markers[index2];
//                                             int size2 = mpSizes[k];
// 
//                                             Dictionary<int, Dictionary<string, Dictionary<int, Thermodynamics>>> dictSize1;
//                                             if (!_markerMarkerDict2.TryGetValue(marker1, out dictSize1)) continue;
//                                             Dictionary<string, Dictionary<int, Thermodynamics>> dictPair2;
//                                             if (dictSize1.TryGetValue(size1, out dictPair2))
//                                             {
//                                                 Dictionary<int, Thermodynamics> dictSize2;
//                                                 if (dictPair2.TryGetValue(marker2, out dictSize2))
//                                                 {
//                                                     Thermodynamics thermScores;
//                                                     if (dictSize2.TryGetValue(size2, out thermScores))
//                                                     {
//                                                         sb.Append(thermScores.LowestThermScore);
//                                                     }
//                                                 }
//                                             }
//                                         }
// 
//                                         sb.AppendLine();
//                                     }
//                                     sb.AppendLine();
//                                 }
//                                 //report the multiplex solution so far
//                                 _bgw.ReportProgress(0, sb.ToString());
                                Pick(multiplexIndex + 1, 0, random, markerIndexList, random.NextDouble() > 0.5 ? _sizes[0] : _sizes[1]);

                                //within the solution matrix then go to the next row start at the 0th column and pick the next gene for the next spot in the solution matrix
                                //else go to the next column in the same row
                            }
                            else if (size < _sizes[_sizes.Count - 2])
                            {
                                if (size == _sizes[_sizes.Count - 3])
                                {
                                    Pick(multiplexIndex, sizeIndex + 1, random, markerIndexList, size + 100);
                                }
                                else
                                {
                                    if (size == _sizes[_sizes.Count - 4])
                                    {
                                        Pick(multiplexIndex, sizeIndex + 1, random, markerIndexList, random.NextDouble() > 0.75 ? size + 100 : size + 150);
                                    }
                                    else if (size == _sizes[_sizes.Count/2 - 1])
                                    {
                                        Pick(multiplexIndex, sizeIndex + 1, random, markerIndexList, random.NextDouble() > 0.5 ? size + 100 : size + 150);
                                    }
                                    else
                                    {
                                        Pick(multiplexIndex, sizeIndex + 1, random, markerIndexList, random.NextDouble() > 0.25 ? size + 100 : size + 150);
                                    }
                                }
                                //pick the next gene for the solution matrix
                            }
                            _solutionMatrix[multiplexIndex][sizeIndex] = tempMarkerPlaceHolder;
                            _sizeMatrix[multiplexIndex][sizeIndex] = tempSizePlaceHolder;
                            //if a suitable gene has not been found for particular spot in the solution matrix then the previous value is restored and picking resumes at this spot
                        }

                //if the i, genes incrementor, reaches the end of the genes list 
                if (currentCandidateMarker == markerIndexList.Count - 1)
                {
                    currentCandidateMarker = 0;
                    //then return the i value to the start of the gene list
                    //else...
                }
                else
                {
                    currentCandidateMarker += 1;
                    //increment through the genes list normally until the i value reaches the random start value, j
                }
            } while (currentCandidateMarker != randomStartForMarkerSearch);
        }


        private void AddMarkerSizesToDicts(int size2, string id2, int size1, string id1)
        {
            if (_sizeMarkerDict.ContainsKey(size1))
            {
                _sizeMarkerDict[size1].Add(id1);
            }
            else
            {
                _sizeMarkerDict.Add(size1, new HashSet<string> {id1});
            }

            if (_sizeMarkerDict.ContainsKey(size2))
            {
                _sizeMarkerDict[size2].Add(id2);
            }
            else
            {
                _sizeMarkerDict.Add(size2, new HashSet<string> {id2});
            }


            if (_markerSizeDict.ContainsKey(id1))
            {
                _markerSizeDict[id1].Add(size1);
            }
            else
            {
                _markerSizeDict.Add(id1, new HashSet<int> {size1});
            }

            if (_markerSizeDict.ContainsKey(id2))
            {
                _markerSizeDict[id2].Add(size2);
            }
            else
            {
                _markerSizeDict.Add(id2, new HashSet<int> {size2});
            }
        }


        private bool CheckMarkerAgainstMultiplex(int multiplexIndex, int sizeIndex, int markerIndex, int size)
        {
            try
            {
                if (sizeIndex == 0) return true;

                string markerToAdd = _markers[markerIndex];
                int markerSize = size;
                if (!_markerSizeDict[markerToAdd].Contains(markerSize)) return false;


                ThermScore score = ThermScore.High;

                for (int i = 0; i < _solutionMatrix[multiplexIndex].Length; i++)
                {
                    int multiplexMarkerIndex = _solutionMatrix[multiplexIndex][i];
                    if (multiplexMarkerIndex == -1) continue;

                    string multiplexMarker = _markers[multiplexMarkerIndex];

                    int multiplexMarkerSize = _sizeMatrix[multiplexIndex][i];

                    ThermScore currentLowestThermScore = _markerMarkerDict2[markerToAdd][markerSize][multiplexMarker][multiplexMarkerSize].LowestThermScore;
                    if (currentLowestThermScore > score)
                    {
                        score = currentLowestThermScore;
                    }
                }
                return score <= _minThermScore;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void AddPrimerPrimerThermodynamicsToDict(double primPrimAny,
                                                         double primProdEnd1,
                                                         double primPrimEnd1,
                                                         double primPrimEnd2,
                                                         double primProdAny,
                                                         string id2,
                                                         string id1,
                                                         int size2,
                                                         int size1)
        {
            if (_markerMarkerDict.ContainsKey(id1))
            {
                if (_markerMarkerDict[id1].ContainsKey(id2))
                {
                    if (_markerMarkerDict[id1][id2].ContainsKey(size1))
                    {
                        if (!_markerMarkerDict[id1][id2][size1].ContainsKey(size2))
                        {
                            _markerMarkerDict[id1][id2][size1].Add(size2,
                                                                   new Thermodynamics(primProdAny,
                                                                                      primProdEnd1,
                                                                                      primPrimAny,
                                                                                      primPrimEnd2,
                                                                                      primPrimEnd1));
                        }
                    }
                    else
                    {
                        _markerMarkerDict[id1][id2].Add(size1,
                                                        new Dictionary<int, Thermodynamics>
                                                            {
                                                                {
                                                                    size2, new Thermodynamics(primProdAny,
                                                                                              primProdEnd1,
                                                                                              primPrimAny,
                                                                                              primPrimEnd2,
                                                                                              primPrimEnd1)
                                                                    }
                                                            });
                    }
                }
                else
                {
                    _markerMarkerDict[id1].Add(id2,
                                               new Dictionary<int, Dictionary<int, Thermodynamics>>
                                                   {
                                                       {
                                                           size1, new Dictionary<int, Thermodynamics>
                                                                      {
                                                                          {
                                                                              size2,
                                                                              new Thermodynamics(primProdAny,
                                                                                                 primProdEnd1,
                                                                                                 primPrimAny,
                                                                                                 primPrimEnd2,
                                                                                                 primPrimEnd1)
                                                                              }
                                                                      }
                                                           }
                                                   });
                }
            }
            else
            {
                _markerMarkerDict.Add(id1,
                                      new Dictionary<string,
                                          Dictionary<int,
                                          Dictionary<int,
                                          Thermodynamics>>>
                                          {
                                              {
                                                  id2,
                                                  new Dictionary<int,
                                                  Dictionary<int,
                                                  Thermodynamics>>
                                                      {
                                                          {
                                                              size1,
                                                              new Dictionary<int,
                                                              Thermodynamics>
                                                                  {
                                                                      {
                                                                          size2,
                                                                          new Thermodynamics(primProdAny,
                                                                                             primProdEnd1,
                                                                                             primPrimAny,
                                                                                             primPrimEnd2,
                                                                                             primPrimEnd1)
                                                                          }
                                                                  }
                                                              }
                                                      }
                                                  }
                                          });
            }


            if (_markerMarkerDict.ContainsKey(id2))
            {
                if (_markerMarkerDict[id2].ContainsKey(id1))
                {
                    if (_markerMarkerDict[id2][id1].ContainsKey(size2))
                    {
                        if (!_markerMarkerDict[id2][id1][size2].ContainsKey(size1))
                        {
                            _markerMarkerDict[id2][id1][size2].Add(size1,
                                                                   new Thermodynamics(primProdAny,
                                                                                      primProdEnd1,
                                                                                      primPrimAny,
                                                                                      primPrimEnd2,
                                                                                      primPrimEnd1));
                        }
                    }
                    else
                    {
                        _markerMarkerDict[id2][id1].Add(size2,
                                                        new Dictionary<int, Thermodynamics>
                                                            {
                                                                {
                                                                    size1, new Thermodynamics(primProdAny,
                                                                                              primProdEnd1,
                                                                                              primPrimAny,
                                                                                              primPrimEnd2,
                                                                                              primPrimEnd1)
                                                                    }
                                                            });
                    }
                }
                else
                {
                    _markerMarkerDict[id2].Add(id1,
                                               new Dictionary<int, Dictionary<int, Thermodynamics>>
                                                   {
                                                       {
                                                           size2, new Dictionary<int, Thermodynamics>
                                                                      {
                                                                          {
                                                                              size1,
                                                                              new Thermodynamics(primProdAny,
                                                                                                 primProdEnd1,
                                                                                                 primPrimAny,
                                                                                                 primPrimEnd2,
                                                                                                 primPrimEnd1)
                                                                              }
                                                                      }
                                                           }
                                                   });
                }
            }
            else
            {
                _markerMarkerDict.Add(id2,
                                      new Dictionary<string,
                                          Dictionary<int,
                                          Dictionary<int,
                                          Thermodynamics>>>
                                          {
                                              {
                                                  id1,
                                                  new Dictionary<int,
                                                  Dictionary<int,
                                                  Thermodynamics>>
                                                      {
                                                          {
                                                              size2,
                                                              new Dictionary<int,
                                                              Thermodynamics>
                                                                  {
                                                                      {
                                                                          size1,
                                                                          new Thermodynamics(primProdAny,
                                                                                             primProdEnd1,
                                                                                             primPrimAny,
                                                                                             primPrimEnd2,
                                                                                             primPrimEnd1)
                                                                          }
                                                                  }
                                                              }
                                                      }
                                                  }
                                          });
            }
        }


        private void AddPrimerPrimerThermodynamicsToDict2(double primPrimAny,
                                                          double primProdEnd1,
                                                          double primPrimEnd1,
                                                          double primPrimEnd2,
                                                          double primProdAny,
                                                          string id2,
                                                          string id1,
                                                          int size2,
                                                          int size1)
        {
            if (_markerMarkerDict2.ContainsKey(id1))
            {
                if (_markerMarkerDict2[id1].ContainsKey(size1))
                {
                    if (_markerMarkerDict2[id1][size1].ContainsKey(id2))
                    {
                        if (!_markerMarkerDict2[id1][size1][id2].ContainsKey(size2))
                        {
                            _markerMarkerDict2[id1][size1][id2].Add(size2,
                                                                    new Thermodynamics(primProdAny,
                                                                                       primProdEnd1,
                                                                                       primPrimAny,
                                                                                       primPrimEnd2,
                                                                                       primPrimEnd1));
                        }
                    }
                    else
                    {
                        _markerMarkerDict2[id1][size1].Add(id2,
                                                           new Dictionary<int, Thermodynamics>
                                                               {
                                                                   {
                                                                       size2, new Thermodynamics(primProdAny,
                                                                                                 primProdEnd1,
                                                                                                 primPrimAny,
                                                                                                 primPrimEnd2,
                                                                                                 primPrimEnd1)
                                                                       }
                                                               });
                    }
                }
                else
                {
                    _markerMarkerDict2[id1].Add(size1,
                                                new Dictionary<string, Dictionary<int, Thermodynamics>>
                                                    {
                                                        {
                                                            id2, new Dictionary<int, Thermodynamics>
                                                                     {
                                                                         {
                                                                             size2,
                                                                             new Thermodynamics(primProdAny,
                                                                                                primProdEnd1,
                                                                                                primPrimAny,
                                                                                                primPrimEnd2,
                                                                                                primPrimEnd1)
                                                                             }
                                                                     }
                                                            }
                                                    });
                }
            }
            else
            {
                _markerMarkerDict2.Add(id1,
                                       new Dictionary<int, Dictionary<string, Dictionary<int, Thermodynamics>>>
                                           {
                                               {
                                                   size1,
                                                   new Dictionary<string, Dictionary<int, Thermodynamics>>
                                                       {
                                                           {
                                                               id2,
                                                               new Dictionary<int,
                                                               Thermodynamics>
                                                                   {
                                                                       {
                                                                           size2,
                                                                           new Thermodynamics(primProdAny,
                                                                                              primProdEnd1,
                                                                                              primPrimAny,
                                                                                              primPrimEnd2,
                                                                                              primPrimEnd1)
                                                                           }
                                                                   }
                                                               }
                                                       }
                                                   }
                                           });
            }

            if (_markerMarkerDict2.ContainsKey(id2))
            {
                if (_markerMarkerDict2[id2].ContainsKey(size2))
                {
                    if (_markerMarkerDict2[id2][size2].ContainsKey(id1))
                    {
                        if (!_markerMarkerDict2[id2][size2][id1].ContainsKey(size1))
                        {
                            _markerMarkerDict2[id2][size2][id1].Add(size1,
                                                                    new Thermodynamics(primProdAny,
                                                                                       primProdEnd1,
                                                                                       primPrimAny,
                                                                                       primPrimEnd2,
                                                                                       primPrimEnd1));
                        }
                    }
                    else
                    {
                        _markerMarkerDict2[id2][size2].Add(id1,
                                                           new Dictionary<int, Thermodynamics>
                                                               {
                                                                   {
                                                                       size1, new Thermodynamics(primProdAny,
                                                                                                 primProdEnd1,
                                                                                                 primPrimAny,
                                                                                                 primPrimEnd2,
                                                                                                 primPrimEnd1)
                                                                       }
                                                               });
                    }
                }
                else
                {
                    _markerMarkerDict2[id2].Add(size2,
                                                new Dictionary<string, Dictionary<int, Thermodynamics>>
                                                    {
                                                        {
                                                            id1, new Dictionary<int, Thermodynamics>
                                                                     {
                                                                         {
                                                                             size1,
                                                                             new Thermodynamics(primProdAny,
                                                                                                primProdEnd1,
                                                                                                primPrimAny,
                                                                                                primPrimEnd2,
                                                                                                primPrimEnd1)
                                                                             }
                                                                     }
                                                            }
                                                    });
                }
            }
            else
            {
                _markerMarkerDict2.Add(id2,
                                       new Dictionary<int, Dictionary<string, Dictionary<int, Thermodynamics>>>
                                           {
                                               {
                                                   size2,
                                                   new Dictionary<string, Dictionary<int, Thermodynamics>>
                                                       {
                                                           {
                                                               id1,
                                                               new Dictionary<int,
                                                               Thermodynamics>
                                                                   {
                                                                       {
                                                                           size1,
                                                                           new Thermodynamics(primProdAny,
                                                                                              primProdEnd1,
                                                                                              primPrimAny,
                                                                                              primPrimEnd2,
                                                                                              primPrimEnd1)
                                                                           }
                                                                   }
                                                               }
                                                       }
                                                   }
                                           });
            }
        }
    }

    public class MultiplexGroups
    {
        private readonly MultiplexGrouping _multiplexGrouping;
        private readonly List<MultiplexGroup> _multiplexes = new List<MultiplexGroup>();

        public MultiplexGroups(MultiplexGrouping multiplexGrouping)
        {
            _multiplexGrouping = multiplexGrouping;

            int[][] solutions = _multiplexGrouping.SolutionMatrix;

            for (int i = 0; i < solutions.Length; i++)
            {
                var dict = new Dictionary<string, int>();
                for (int j = 0; j < solutions[j].Length; j++)
                {
                    int markerIndex = solutions[i][j];
                    int size = _multiplexGrouping.SizeMatrix[i][j];
                    string marker = _multiplexGrouping.Markers[markerIndex];

                    dict.Add(marker, size);
                }
                _multiplexes.Add(new MultiplexGroup(dict));
            }
        }

        public MultiplexGrouping MultiplexGrouping { get { return _multiplexGrouping; } }
        public List<MultiplexGroup> Multiplexes { get { return _multiplexes; } }
    }


    public class MultiplexGroup
    {
        private readonly Dictionary<string, int> _markerSizeDict = new Dictionary<string, int>();
        private readonly Dictionary<int, string> _sizeMarkerDict = new Dictionary<int, string>();

        public MultiplexGroup(Dictionary<string, int> markerSizes)
        {
            foreach (var pair in markerSizes)
            {
                _markerSizeDict.Add(pair.Key, pair.Value);
                _sizeMarkerDict.Add(pair.Value, pair.Key);
            }
        }

        public Dictionary<string, int> MarkerSizeDict { get { return _markerSizeDict; } }
        public Dictionary<int, string> SizeMarkerDict { get { return _sizeMarkerDict; } }
    }
}