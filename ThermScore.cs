namespace PrimerMultiplexGrouper
{
    public enum ThermScore
    {
        High,
        Normal,
        Low,
        VeryLow
    }
}