namespace PrimerMultiplexGrouper
{
    public class Thermodynamics
    {
        private readonly double _primPrimAny;
        private readonly double _primPrimEnd1;
        private readonly double _primPrimEnd2;
        private readonly double _primProdAny;
        private readonly double _primProdEnd1;
        private ThermScore _lowestThermScore;
        private ThermScore _primPrimAnyThermScore;

        private ThermScore _primPrimEnd1ThermScore;
        private ThermScore _primPrimEnd2ThermScore;
        private ThermScore _primProdAnyThermScore;
        private ThermScore _primProdEnd1ThermScore;


        public Thermodynamics(double primProdAny,
                              double primProdEnd1,
                              double primPrimAny,
                              double primPrimEnd2,
                              double primPrimEnd1)
        {
            _primProdAny = primProdAny;
            _primProdEnd1 = primProdEnd1;
            _primPrimAny = primPrimAny;
            _primPrimEnd2 = primPrimEnd2;
            _primPrimEnd1 = primPrimEnd1;
            CalculateThermScores();
        }

        public double PrimPrimEnd1 { get { return _primPrimEnd1; } }

        public double PrimPrimEnd2 { get { return _primPrimEnd2; } }

        public double PrimPrimAny { get { return _primPrimAny; } }

        public double PrimProdEnd1 { get { return _primProdEnd1; } }

        public double PrimProdAny { get { return _primProdAny; } }

        public ThermScore PrimPrimEnd1ThermScore { get { return _primPrimEnd1ThermScore; } }

        public ThermScore PrimPrimEnd2ThermScore { get { return _primPrimEnd2ThermScore; } }

        public ThermScore PrimPrimAnyThermScore { get { return _primPrimAnyThermScore; } }

        public ThermScore PrimProdEnd1ThermScore { get { return _primProdEnd1ThermScore; } }

        public ThermScore PrimProdAnyThermScore { get { return _primProdAnyThermScore; } }

        public ThermScore LowestThermScore { get { return _lowestThermScore; } }

        private void CalculateThermScores()
        {
            //PrimPrimEnd2,	PrimPrimEnd1,	PrimPrimAny,	PrimProdEnd1,	PrimProdAny

            //PrimPrimEnd2
            if (_primPrimEnd2 >= -2)
            {
                _primPrimEnd2ThermScore = ThermScore.High;
            }
            else if (_primPrimEnd2 >= -4)
            {
                _primPrimEnd2ThermScore = ThermScore.Normal;
            }
            else if (_primPrimEnd2 >= -6)
            {
                _primPrimEnd2ThermScore = ThermScore.Low;
            }
            else
            {
                _primPrimEnd2ThermScore = ThermScore.VeryLow;
            }

            //PrimPrimEnd1
            if (_primPrimEnd1 >= -6)
            {
                _primPrimEnd1ThermScore = ThermScore.High;
            }
            else if (_primPrimEnd1 >= -8)
            {
                _primPrimEnd1ThermScore = ThermScore.Normal;
            }
            else if (_primPrimEnd1 >= -10)
            {
                _primPrimEnd1ThermScore = ThermScore.Low;
            }
            else
            {
                _primPrimEnd1ThermScore = ThermScore.VeryLow;
            }

            //PrimPrimAny
            if (_primPrimAny >= -6)
            {
                _primPrimAnyThermScore = ThermScore.High;
            }
            else if (_primPrimAny >= -8)
            {
                _primPrimAnyThermScore = ThermScore.Normal;
            }
            else if (_primPrimAny >= -10)
            {
                _primPrimAnyThermScore = ThermScore.Low;
            }
            else
            {
                _primPrimAnyThermScore = ThermScore.VeryLow;
            }

            //PrimProdEnd1
            if (_primProdEnd1 >= -10)
            {
                _primProdEnd1ThermScore = ThermScore.High;
            }
            else if (_primProdEnd1 >= -12)
            {
                _primProdEnd1ThermScore = ThermScore.Normal;
            }
            else if (_primProdEnd1 >= -14)
            {
                _primProdEnd1ThermScore = ThermScore.Low;
            }
            else
            {
                _primProdEnd1ThermScore = ThermScore.VeryLow;
            }

            //PrimProdAny
            if (_primProdAny >= -10)
            {
                _primProdAnyThermScore = ThermScore.High;
            }
            else if (_primProdAny >= -12)
            {
                _primProdAnyThermScore = ThermScore.Normal;
            }
            else if (_primProdAny >= -14)
            {
                _primProdAnyThermScore = ThermScore.Low;
            }
            else
            {
                _primProdAnyThermScore = ThermScore.VeryLow;
            }

            if (_primPrimAnyThermScore == ThermScore.VeryLow ||
                _primPrimEnd1ThermScore == ThermScore.VeryLow ||
                _primPrimEnd2ThermScore == ThermScore.VeryLow ||
                _primPrimEnd2ThermScore == ThermScore.VeryLow ||
                _primProdAnyThermScore == ThermScore.VeryLow)
            {
                _lowestThermScore = ThermScore.VeryLow;
                return;
            }

            if (_primPrimAnyThermScore == ThermScore.Low ||
                _primPrimEnd1ThermScore == ThermScore.Low ||
                _primPrimEnd2ThermScore == ThermScore.Low ||
                _primPrimEnd2ThermScore == ThermScore.Low ||
                _primProdAnyThermScore == ThermScore.Low)
            {
                _lowestThermScore = ThermScore.Low;
                return;
            }

            if (_primPrimAnyThermScore == ThermScore.Normal ||
                _primPrimEnd1ThermScore == ThermScore.Normal ||
                _primPrimEnd2ThermScore == ThermScore.Normal ||
                _primPrimEnd2ThermScore == ThermScore.Normal ||
                _primProdAnyThermScore == ThermScore.Normal)
            {
                _lowestThermScore = ThermScore.Normal;
                return;
            }
            _lowestThermScore = ThermScore.High;
        }
    }
}