using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

//frmMultiplexGrouping is responsible for handling the userform for multiplex grouping
public class frmMultiplexGrouping : Form
{


	private MultiplexPicking objMP = new MultiplexPicking();
	private void btnBrowseScoreFile_Click(System.Object sender, System.EventArgs e)
	{
		sslProgress.Text = "Ready";
		var _with1 = OpenFileDialog1;
		_with1.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
		_with1.FilterIndex = 1;
		_with1.Multiselect = false;
		// Call the ShowDialog method to show the dialogbox.
		if (OpenFileDialog1.ShowDialog == System.Windows.Forms.DialogResult.OK) {
			try {
				txtScoreFile.Text = OpenFileDialog1.FileName;
			} catch (Exception ex) {
				Interaction.MsgBox(ex.Message, MessageBoxButtons.OK, My.Application.Info.Title);
			}

		}
	}

	private void btnBrowseSave_Click(System.Object sender, System.EventArgs e)
	{
		sslProgress.Text = "Ready";
		var _with2 = SaveFileDialog1;
		_with2.AddExtension = true;
		_with2.FileName = Strings.Replace(System.DateTime.Today, "/", "-") + "-MultiplexGrouping-" + txtMultiplexes.Text + "MP-" + txtSolutions.Text + "solutions.txt";
		//xlsx"
		_with2.DefaultExt = ".txt";
		// ".xlsx"
		_with2.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
		//"Excel 2007 (*.xlsx)|*.xlsx|All Files (*.*)|*.*"
		_with2.FilterIndex = 1;

		if (SaveFileDialog1.ShowDialog == System.Windows.Forms.DialogResult.OK) {
			try {
				txtSavePath.Text = SaveFileDialog1.FileName;
			} catch (Exception ex) {
				Interaction.MsgBox(ex.Message, MessageBoxButtons.OK, My.Application.Info.Title);
			}
		}

	}

	private void frmMultiplexGrouping_Load(System.Object sender, System.EventArgs e)
	{
		sslProgress.Text = "Ready";
	}

	private void btnRun_Click(System.Object sender, System.EventArgs e)
	{

		if (txtSavePath.Text != Constants.vbNullString & txtScoreFile.Text != Constants.vbNullString & Information.IsNumeric(txtMultiplexes.Text) & Information.IsNumeric(txtSolutions.Text)) {
			objMP.MultiplexMain(txtScoreFile.Text, txtSavePath.Text, txtMultiplexes.Text, txtSolutions.Text);
		}
	}

	private void txtMultiplexes_TextChanged(System.Object sender, System.EventArgs e)
	{
		sslProgress.Text = "Ready";
		if (!Information.IsNumeric(txtMultiplexes.Text) & !(txtMultiplexes.Text == Constants.vbNullString)) {
			Interaction.MsgBox("Numeric values only for number of multiplexes.");
			txtMultiplexes.Text = "4";
		}
	}

	private void txtSolutions_TextChanged(System.Object sender, System.EventArgs e)
	{
		sslProgress.Text = "Ready";
		if (!Information.IsNumeric(txtSolutions.Text) & !(txtMultiplexes.Text == Constants.vbNullString)) {
			Interaction.MsgBox("Numeric values only for number of solutions.");
			txtSolutions.Text = "1";
		}
	}
	public frmMultiplexGrouping()
	{
		Load += frmMultiplexGrouping_Load;
	}

}

//MultiplexPicking is responsible for the grouping of compatible genes into multiplexes
public class MultiplexPicking
{

		//structure/collection for holding gene comparison information - gene index numbers, sizes, thermodynamics
	private GeneCollection objGenes = new GeneCollection();
		//2D array for holding gene index numbers
	private int[,] matrix;
		//non-redundant list of genes
	private string[] genes;
		//non-redundant list of sizes, e.g. 100, 200, 300, 400, 500
	private int[] sizes;
		//number of solutions currently solved
	private int NumberOfSolutions;
		//boolean for determining if a solution has been achieved; false if there is no current solution, true if solution has been found and numberOfSolutions is incremented
	private bool solved;
		//number of gene comparisons in objGenes GeneCollection
	private long geneInfoNum;
		//number of multiplexes in each solution
	private int multiplexes;

	//MultiplexMain - Main subroutine for handling picking of genes for multiplexes
	//scoreFilePath - File path for scorefile.txt -> frmMultiplexGrouping.txtScoreFile.Text
	//saveFilePath - File path for saving new multiplex picks -> frmMultiplexGrouping.txtSaveFile.Text
	//m - Number of multiplexes in each solution -> frmMultiplexGrouping.txtMultiplexes.Text
	//solutions - Number of solutions for multiplex picking to return in text file -> frmMultiplexGrouping.txtSolutions.Text
	public void MultiplexMain(string scoreFilePath, string saveFilePath, int m, int solutions)
	{
		//Filter scorefile.txt for redundancy in sizes and genes; return structure with all relevant sorted information
		ScoreFileFilter(scoreFilePath, objGenes);

		multiplexes = m;
		NumberOfSolutions = 0;

		try {
			do {
				matrix = new int[multiplexes, sizes.Length];
				//resize 'matrix'; number of multiplexes by number of genes in each multiplex
				for (int i = 0; i <= multiplexes - 1; i++) {
					for (int j = 0; j <= sizes.Length - 1; j++) {
						matrix[i, j] = -1;
						//set all elements in 'matrix' to '-1'
					}
				}
				//Display the current solution being worked on
				frmMultiplexGrouping.sslProgress.Text = "Working on solution " + Convert.ToString(NumberOfSolutions + 1);
				frmMultiplexGrouping.Refresh();
				//refresh form
				solved = false;
				Pick(0, 0);
				//start picking genes for first spot in 'matrix' 2D array
			} while (!(NumberOfSolutions == solutions));
			//do until the number of solutions solved equals the number of desired solutions

			//Deinitialize arrays and objects
			NumberOfSolutions = 0;
			genes = null;
			sizes = null;
			matrix = null;
			objGenes.Clear();
			GC.Collect();

			frmMultiplexGrouping.sslProgress.Text = "Complete - " + Convert.ToString(solutions) + " Solution(s) Generated";

		} catch (Exception ex) {
			Interaction.MsgBox(ex.Message, MsgBoxStyle.Exclamation, My.Application.Info.Title);
		}
	}

	//Pick - Sub for picking the genes to be present in each element of the multiplex solution with recursion and backtracing until suitable solution is found
	//c - Column number referring to x in matrix(x,y)
	//r - Row number referring to y in matrix(x,y)
	private void Pick(int c, int r)
	{
		int i = 0;
		//increments through genes
		int j = 0;
		//randomly selected spot to start looking in genes 
		int t = 0;
		//temporary holder for backtracing
		if (solved == false) {
			if (r > multiplexes - 1) {
				WritePrimerInfo();
				//if all elements in 'matrix' are filled then a solution has been found and will be written
			} else {
				j = Conversion.Int((genes.Length - 1) * VBMath.Rnd());
				//randomly select spot to start looking in genes
				i = j;
				do {
					//check if pick is suitable for multiplex solution 
					if (SafePick(c, r, i) == true) {
						t = matrix[r, c];
						//temporarily store the previous value in the current solution matrix in t, a temporary variable
						matrix[r, c] = i;
						//i, representing a gene pick that is suitable is placed within the current spot within the solution matrix
						//if the current column number, c, within the solution matrix reaches the upper bound of the columns 
						if (c == sizes.Length - 1) {
							Pick(0, r + 1);
							//within the solution matrix then go to the next row start at the 0th column and pick the next gene for the next spot in the solution matrix
						//else go to the next column in the same row
						} else {
							Pick(c + 1, r);
							//pick the next gene for the solution matrix
						}
						matrix[r, c] = t;
						//if a suitable gene has not been found for particular spot in the solution matrix then the previous value is restored and picking resumes at this spot
					}

					//if the i, genes incrementor, reaches the end of the genes list 
					if (i == genes.Length - 1) {
						i = 0;
						//then return the i value to the start of the gene list
					//else...
					} else {
						i += 1;
						//increment through the genes list normally until the i value reaches the random start value, j
					}
				} while (!(i == j));
			}
		}
	}

	//SafePick - Determines whether the current gene pick candidate is suitable for the solution matrix
	//col    - Column number in the solution matrix -> x in matrix(x,y)
	//row    - Row number in the solution matrix -> y in matrix(x,y)
	//g      - Current gene pick candidate index number corresponding to gene in 'genes'
	//Returns True if the pick passes the tests; False if it doesn't pass the compatibility tests
	private bool SafePick(long col, long row, long g)
	{
		bool sp = false;
		//pick is by default not compatible
		//Number of solutions provided is not equal to the number of solutions wanted by the user then sub continues
		//If the current solution has not been solved yet then proceed with the following procedure
		if (solved == false) {
			//Check for presence of the gene within the multiplex already; redundancy
			for (int i = 0; i <= multiplexes - 1; i++) {
				for (int j = 0; j <= 4; j++) {
					if (g == matrix[i, j]) {
						return false;
					}
				}
			}

			//Check for size availability in thermodynamics scorefile
			long size = 0;
			size = sizes[col];
			for (long j = 0; j <= geneInfoNum - 1; j++) {
				if (g == objGenes[j].Gene1) {
					if (size == objGenes[j].GeneSize1) {
						sp = true;
						break; // TODO: might not be correct. Was : Exit For
					}
				} else if (g == objGenes[j].Gene2) {
					if (size == objGenes[j].GeneSize2) {
						sp = true;
						break; // TODO: might not be correct. Was : Exit For
					}
				}
			}

			//If gene pick is not available in the thermodynamic score file then exit the function and try a different pick
			if (sp == false) {
				return false;
			}

			//Check that gene is not within 5 genes of another in the assay already; not from the same cluster; eliminate cluster bias
			string tmpG = null;
			//Temporary variable to hold the gene name string of the candidate gene
			long pid = 0;
			//Protein ID (PID) number for the candidate gene
			string m = null;
			//Temporary variable to hold the gene name string of the gene in the solution matrix being compared against the candidate gene
			long mpid = 0;
			//PID of the gene in the solution matrix

			//Extract PID number from gene name string
			tmpG = Strings.Left(genes[g], Strings.InStrRev(genes[g], "_") - 1);
			tmpG = Strings.Mid(tmpG, Strings.InStrRev(tmpG, "_") + 1);
			pid = Convert.ToInt64(tmpG);
			for (int i = 0; i <= multiplexes - 1; i++) {
				for (int j = 0; j <= 4; j++) {
					if (matrix[i, j] != -1) {
						m = genes[matrix[i, j]];
						//Extract PID number from gene name string
						tmpG = Strings.Left(m, Strings.InStrRev(m, "_") - 1);
						tmpG = Strings.Mid(tmpG, Strings.InStrRev(tmpG, "_") + 1);
						mpid = Convert.ToInt64(tmpG);
						//Check if the genes are within 5 genes of each other; if they are then the candidate gene is not a suitable pick, else candidate still considered
						if (mpid >= pid - 5 & mpid <= pid + 5) {
							return false;
						}
					}
				}
			}

			//Check thermodynamics; normal stringency with all other genes in that particular multiplex; score of 2 or lower 
			byte maxTS = 0;
			//Largest thermodynamics score of gene comparison; if 0 then candidate gene does not pass - no comparison between candidate gene and gene in solution matrix
			int sizeM = 0;
			//Size of the gene in the matrix
			int ml = 0;
			//Gene index number of gene in solution matrix
			bool noMatch = false;
			//False if there is a thermodynamics comparison between candidate gene and gene in solution matrix, True if there isn't

			for (byte j = 0; j <= 4; j++) {
				noMatch = false;
				if (matrix[row, j] != -1) {
					noMatch = true;
					ml = matrix[row, j];
					sizeM = sizes[j];
					//Search for gene comparison in thermodynamics comparisons
					for (long k = 0; k <= geneInfoNum - 1; k++) {
						if (objGenes[k].Gene1 == ml & objGenes[k].GeneSize1 == sizeM) {
							if (objGenes[k].Gene2 == g & objGenes[k].GeneSize2 == size) {
								if (objGenes[k].ThermodynamicsScore > maxTS) {
									maxTS = objGenes[k].ThermodynamicsScore;
								}
								noMatch = false;
								break; // TODO: might not be correct. Was : Exit For
							}
						} else if (objGenes[k].Gene2 == ml & objGenes[k].GeneSize2 == sizeM) {
							if (objGenes[k].Gene1 == g & objGenes[k].GeneSize1 == size) {
								if (objGenes[k].ThermodynamicsScore > maxTS) {
									maxTS = objGenes[k].ThermodynamicsScore;
								}
								noMatch = false;
								break; // TODO: might not be correct. Was : Exit For
							}
						}

					}
				}
				if (maxTS > 2)
					break; // TODO: might not be correct. Was : Exit For
				//if the thermodynamics score is greater than 2 then the candidate gene is not a good match for the solution
				//if there is no match then the maxTS value is set to 0
				if (noMatch == true) {
					maxTS = 0;
					break; // TODO: might not be correct. Was : Exit For
				}
			}

			//Determine if the thermodynamics score passes or if the comparison is even present or if the gene candidate is the first to fill spot in multiplex
			if (maxTS == 0) {
				if (matrix[row, 0] == -1) {
					return true;
					//if the first gene to be placed into a multiplex then gene candidate passes
				}
				return false;
				//if not the first gene to be placed into a multiplex and there is no thermodynamic comparison then gene candidate fails
			//normal to high thermodynamic compatibility passes
			} else if (maxTS < 3) {
				return true;
			//low thermodynamic compatibilty fails
			} else if (maxTS > 2) {
				return false;
			}
		}
		//return true if good pick or false if not a good pick
		return false;
	}

	//WritePrimerInfo - Sub that writes the multiplex grouping solutions to a text file with primer information
	//Each new solution is appended to the file with each solution delimited by a semicolon, ';'

	private void WritePrimerInfo()
	{
		string[] gene = null;
		//String array for holding gene names
		int[] gSize = null;
		//Integer array for holding the size of a gene product
		string[] fPrimer = null;
		string[] rPrimer = null;
		string[] pPrimer = null;

		try {
			ReadPrimerFile(ref gene, ref gSize, ref fPrimer, ref rPrimer, ref pPrimer);
		} catch (Exception ex) {
			Interaction.MsgBox(ex.Message, MsgBoxStyle.Exclamation, My.Application.Info.Title);
		}

		//Multiplex grouping solution has been reached so...
		solved = true;
		NumberOfSolutions += 1;

		//Open up a file writer for the new file containing the multiplex grouping solutions
		dynamic writer = My.Computer.FileSystem.OpenTextFileWriter(frmMultiplexGrouping.txtSavePath.Text, true);

		//Solution <number>
		writer.WriteLine("Solution" + Constants.vbTab + Convert.ToString(NumberOfSolutions));

		//Header for each solution -> Multiplex  Size    Gene    Forward Primer  Reverse Primer  Product Length  Primer Product
		writer.WriteLine("Multiplex" + Constants.vbTab + "Size" + Constants.vbTab + "Gene" + Constants.vbTab + "Forward Primer" + Constants.vbTab + "Reverse Primer" + Constants.vbTab + "Product Length" + Constants.vbTab + "Primer Product");

		int currentSize = 0;
		//Temporary variable holding gene size of current gene in solution matrix
		string tmpM = null;
		//Temporary variable holding gene name of current gene in solution matrix

		for (byte i = 0; i <= multiplexes - 1; i++) {
			for (byte j = 0; j <= sizes.Length - 1; j++) {
				currentSize = sizes[j];
				tmpM = genes[matrix[i, j]];
				for (long k = 0; k <= gene.Length - 1; k++) {
					if (currentSize == gSize[k]) {
						if (tmpM == gene[k]) {
							writer.WriteLine(Convert.ToString(i + 1) + Constants.vbTab + Convert.ToString(currentSize) + Constants.vbTab + tmpM + Constants.vbTab + fPrimer[k] + Constants.vbTab + rPrimer[k] + Constants.vbTab + Convert.ToString(Strings.Len(pPrimer[k])) + Constants.vbTab + pPrimer[k]);
							break; // TODO: might not be correct. Was : Exit For
						}
					}
				}
			}
		}

		writer.WriteLine(";");
		writer.Close();

		GC.Collect();

	}

}
